from requests import get
# import sys

texto = "AAAAAABBBBBCCCCDDDEEF"
link = "https://gitlab.com/cesardddp/algoritmo-compressao-codificacao-de-shannon-fano/-/raw/master/texto.exemplo"
texto = f'{get(link).content}'  #texto a ser codificado
# sys.getsizeof(texto)


#CALCULA FREQUENCIA DE CADA PALAVRA EM UM CONJUNTO DE DADOS

dados = texto#"ESTE É UM TEXTO AEATÓIO QUE ESCREVI PPRA TERSTAR MEU CODIGO"


simbolo_frequencia={"tam_amostra":0}

for dado in dados:#PARA CADA DADO NO CONJUNTO (PARA CADA LETRA NA STRING)
  simbolo_frequencia[dado] = simbolo_frequencia.setdefault(dado,0)+1  
  simbolo_frequencia["tam_amostra"]+=1

tam_amostra = simbolo_frequencia.pop("tam_amostra")
print(simbolo_frequencia)
bits=tam_amostra*8
print("tamanho em bits gasto: ", bits)


#MONTANDO A ARVORE BINARIA PARA SHANNON-FANO
# simbolo_frequencia = {'F': 1, 'A': 6, 'B': 5, 'C': 4, 'D': 3, 'E': 2}

#primeiro transformo o dicionario em uma lista de tuplas.
#As tuplas contem o par SIMBOLO e FREQUENCIA
lista_dado_frequencia = sorted( [ (dado,freq) for dado,freq in simbolo_frequencia.items()])



def laço(raiz:dict,lista_dado_frequencia:list):
  #condição de para da função recursiva
  #caso a lista do nó atual contenha apenas um elemento, retornar o SIMBOLO
  #isso fará que o nó corrente assuma esse simbolo
  if lista_dado_frequencia.__len__() == 1:
   return lista_dado_frequencia[0][0]
 
  #caso a lista neste nó seja uma lista (lentgh > 1) o algoritimo continua
  
  #calcula a soma das ocorrencias do simbolos da lista no nó atual
  #e recalcula probabilidade de occorencia dos simbolos da lista no nó atual 
  # e coloca na lista probs_lista
  tam = sum(map( lambda x : x[1] ,lista_dado_frequencia)) 
  probs_list = list( map( lambda x : x[1]/tam ,lista_dado_frequencia) )#
  #OBS: ambas listas estão ordenadas descrescente frequencia dos simbolos

  # lista_dado_frequencia [C(4),D(3),E(2),F(1)]
  # tam                   10
  # probs_list     [0.4, 0.3, 0.2, 0.1]

  #começa etapas da divisão da lista em duas listas de probald parecidas

  #inicia a lista que irá para o novo nó esquerdo 
  lista1 = []
  #inicia lista que irá para o nó direito
  lista2 = lista_dado_frequencia#[C,D,E,F]

  # distancia e N_distancia garantem que ambas a listas tenho probabilidade de
  # ocorrencia de simbolos proximas de 50%
  distancia =  sum(  probs_list[len(lista1):len(lista2)] ) - sum( probs_list[:len(lista1)] ) #pego a distancia entre as duas sum frequencias
  N_distancia = sum(  probs_list[len(lista1)+1:len(lista2)] ) - sum(probs_list[:len(lista1)+1])

  #itera sobre a lista verificando se, ao passar o elemento mais a esquerda da
  # lista2 para a lista1, as listas têm probabilidade de ocorrencias o mais 
  # parecido possivel e próximo de 50%

  for i in range(len(lista_dado_frequencia)):
    if  N_distancia < distancia and N_distancia*-1 < distancia  :
      distancia = N_distancia
      lista1.append(lista2.pop(0))
      N_distancia = sum(  probs_list[len(lista1)+1:len(lista2)] ) - sum(probs_list[:len(lista1)+1])

  #por fim, é criado um nó esquerdo e um direito e feito uma chamada recursiva 
  # desta função passando as duas novas lista geradas

  # este if itera sobre o lado esquerdo da arvore caso a lista possua elementos
  if lista1:
    raiz.setdefault("esquerda",{})
    raiz.update(esquerda=laço(raiz["esquerda"],lista1))
  # este if itera sobre o lado direito da arvore caso a lista possua elementos
  if lista2:
    raiz.setdefault("direita",{})
    raiz.update(direita=laço(raiz["direita"],lista2))

  #enfim retorna esta subarvore para a instancia que chamou este laço da recursão
  return raiz

#define a arvore e sua raiz
arvore ={ "raiz":{} }

#chama a função pasando a raiz e a lista de tuplas (SIMBOLO,FREQUENCIA)
laço(arvore["raiz"],lista_dado_frequencia)

from pprint import pprint
pprint(arvore)

#esta função apenas "varre" a arvore calculando os novos simbolos para os anigos
def varre(raiz:dict,cod=''):
  if isinstance(raiz,dict):
    keys = list(raiz.keys())
    if len(keys) == 2:
      varre(raiz[keys[0]],cod=cod+'0')
      varre(raiz[keys[1]],cod=cod+'1')
    if len(keys)==1:
      varre(raiz[keys[0]])
  else:
    print(raiz,cod)
    codigo.append((raiz,cod))

  return codigo

codigo=[]
varre(arvore)

#este trecho calcula o tam em bits gasto pela nova representação
tam=0
for i in codigo:
  tam+=simbolo_frequencia[i[0]]*len(i[1])

print("tamanho original: ",bits," Tamanho codifcado: ",tam," Taxa de compressão: ",f"{tam/bits*100}%")
